//
//  Webservice.swift
//  The Movie
//
//  Created by rafiul hasan on 3/11/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import Foundation

class MovieAPIService {
    static let sharedInatance  = MovieAPIService()
    func getMovieFromServer(completion: @escaping ([MovieDataModel]?, Error?) -> ()) {
        let baseUrl = "https://api.androidhive.info/json/movies.json"
        
        guard let url = URL(string: baseUrl) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let err = error {
                completion(nil, err)
                print(err.localizedDescription)
            } else {
                guard let data = data else { return }
                do {
                    var movieArrayData = [MovieDataModel]()
                    let jsonDecoder = JSONDecoder()
                    let result = try jsonDecoder.decode([MovieDataModel].self, from: data)
                    for movie in result {
                        movieArrayData.append(movie)
                    }
                    completion(movieArrayData, nil)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        task.resume()
    }

}
