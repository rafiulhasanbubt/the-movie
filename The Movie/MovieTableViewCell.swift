//
//  MovieTableViewCell.swift
//  The Movies
//
//  Created by rafiul hasan on 10/2/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var genLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellView.layer.cornerRadius = 8.0
        cellView.layer.borderColor = UIColor.lightGray.cgColor
        cellView.layer.borderWidth = 0.6
    }

    func getUIdata(movie: MovieDataModel) {
        titleLabel.text = movie.title
        releaseLabel.text = String(movie.releaseYear)
        ratingLabel.text = String(movie.rating)
        genLabel.text = movie.genre[0]
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
