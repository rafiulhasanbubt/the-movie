//
//  ViewController.swift
//  The Movie
//
//  Created by rafiul hasan on 10/8/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var movieVMArray = [MovieViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        self.getData()
    }
    
    func getData(){
        MovieAPIService.sharedInatance.getMovieFromServer { (movies, error) in
            if error == nil {
                self.movieVMArray = movies?.map({ return MovieViewModel(movie: $0)
                }) ?? []
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieVMArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! MovieTableViewCell
        let MVVM = movieVMArray[indexPath.row]
        cell.titleLabel.text = MVVM.title
        cell.releaseLabel.text = "Release: \(String(MVVM.releaseYear!))"
        cell.ratingLabel.text = "Rating: \(String(MVVM.rating!))"
        cell.genLabel.text = "Genre: \(MVVM.genre![0])"
        
        guard let ImageURL = URL(string: MVVM.image!) else { return cell  }
        let data = try? Data(contentsOf: ImageURL)

        if let imageData = data {
            let image = UIImage(data: imageData)
            cell.movieImageView.image = image
        }
        return cell
    }
    
}
