//
//  MovieViewModel.swift
//  The Movie
//
//  Created by rafiul hasan on 3/11/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import Foundation

struct MovieViewModel {    
    var title: String?
    var image: String?
    var rating: Double?
    var releaseYear: Int?
    var genre: [String]?
    
    // Dependent injection
    init(movie: MovieDataModel) {
        self.title = movie.title
        self.image = movie.image
        self.rating = movie.rating
        self.releaseYear = movie.releaseYear
        self.genre = movie.genre
    }
}
