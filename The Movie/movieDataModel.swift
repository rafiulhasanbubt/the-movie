//
//  movieDataModel.swift
//  The Movies
//
//  Created by rafiul hasan on 10/2/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import Foundation

// MARK: - MovieDataModelElement
struct MovieDataModel: Codable {
    let title: String
    let image: String
    let rating: Double
    let releaseYear: Int
    let genre: [String]
}

typealias MovieData = ([MovieDataModel]?) -> Void


class MovieAPI {
    func getMovieFromServer() {
        let baseUrl = "https://api.androidhive.info/json/movies.json"
        
        guard let url = URL(string: baseUrl) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return
            }
            //guard let data = data else { return }
//            do {
//                //create json object from data
//                if let json = try JSONSerialization.jsonObject(with: dataResponse) as? [String: Any] {
//                    if let status = json["status"] as? Bool, status == true {
//                        if let content = json["content"] as? [[String: Any]] {
//                            for category in content {
//                                let title = category["title"]
//                                let image =  category["image"]
//                                let rating =  category["rating"]
//                                let releaseYear =  category["releaseYear"]
//                                let generic =  category["genre"]
//                                print(title , image, rating,releaseYear,generic)
//                            }
//                        }
//                    }
//                }
//
//            } catch let error {
//                print(error.localizedDescription)
//            }

             do{
             //here dataResponse received from a network request
             let jsonResponse = try JSONSerialization.jsonObject(with: dataResponse, options: [])
             guard let jsonArray = jsonResponse as? [String: Any] else { return }
             let movie = self.getMovieData(json: jsonArray)
                print(movie)
                DispatchQueue.main.async {
                   
                }                
             //                 print(jsonArray) //Response result
             //                let jsonDecoder = JSONDecoder()
             //                let result = try jsonDecoder.decode([MovieDataModel].self, from: dataResponse)
             //                let movieData = getMovieFromServer(completion: result])
             //                print(result)
             } catch let parsingError {
             print("Error", parsingError)
             }
        }
        
        task.resume()
    }
    
    func getMovieData(json: [String: Any]) -> MovieDataModel {
        let title = json["title"] as? String ?? ""
        let image = json["image"] as? String ?? ""
        let rating = json["rating"] as? Double ?? 0.0
        let releaseYear = json["releaseYear"] as? Int ?? 0
        let generic = json["genre"] as? [String] ?? [String]()
        
        return MovieDataModel(title: title, image: image, rating: rating, releaseYear: releaseYear, genre: generic)
    }
}
